import { MiniError } from "./MiniError.js";

export class ForbiddenError extends MiniError {
    constructor(msg = "Insufficient permissions", cause) {
        super(msg, cause);
    }

    get httpStatusCode() {
        return 403;
    }
}
