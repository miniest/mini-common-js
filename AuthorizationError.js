import { MiniError } from "./MiniError.js";

export class AuthorizationError extends MiniError {
    constructor(msg = "Unauthorised access", cause) {
        super(msg, cause);
    }

    get httpStatusCode() {
        return 401;
    }
}
