import mongodb from "mongodb";
import { MiniError } from "./MiniError.js";

let client; // instance of initialized client
let defaultDbName; // name set during init

export class MongoTools {
    static async init(url, dbName) {
        // TODO dbName is required
        // TODO maybe we should prevent init if client is already not null
        client = await mongodb.MongoClient.connect(url, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
        });
    }

    static getClient() {
        if (client) return client;

        throw new MiniError("Not initialized yet!"); // TODO change to mini error
    }

    /**
     * Get instance of database. If no name is provided, it returns default database defined
     * during init.
     *
     * @param {String} dbName - name of database. If omitted, default database set during init is returned
     *
     * @returns Mongo database instance
     */
    static getDb(dbName) {
        return MongoTools.getClient().db(dbName || defaultDbName);
    }
}
