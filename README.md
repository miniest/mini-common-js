# mini-common-js

Common tools for miniest people.

## Server

If you want to use Server module you must manually include `express` into your dependencies. It is intentionally not listed in `peerDependencies` because not every user of `mini-common` is interested in the Server module.

## MiniError

Error class with specified http status code. By default returns 500. For other types of error see the classes extended from MiniError.

## AuthorizationError

Error extended from MiniError, should be used with unauthorized access/unauthenticated user cases. Its http status code is 401.

## ForbiddenError

Error extended from MiniError, should be used in case of missing permissions. Its http status code is 403.


# Development

## Commit message

We follow conventional changelog patter for commit messages.

See [https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular].

We preffer these prefixes:
* feat: new feature added (will be seen in changelog)
* fix: fix of existing code (will be seen in changelog)
* perf: changed performance of some code (will be seen in changelog)
* build: build system and ci related stuff (not seen in changelog)
* docs: documentation related stuff (not seen in changelog)
* test: tests related stuff (not seen in changelog)

If you cannot determine correct prefix for your change simple do commit without a prefix. Try to use feat/fix/perf prefixes
for any user related stuff to let enduser know about the change.
