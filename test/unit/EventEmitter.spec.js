/* eslint-env mocha */

import chai from "chai";
import { EventEmitter } from "../../EventEmitter.js";

const assert = chai.assert;

describe("EventEmitter", function() {
    it("should instantiate", function() {
        const ee = new EventEmitter();

        assert.instanceOf(ee, EventEmitter);
    });

    it("should register handler and emit", function() {
        const ee = new EventEmitter();

        let called = 0;

        const handler = function(data) {
            ++called;
        };

        ee.on("event", handler);
        ee.emit("event");
        ee.emit("event");

        assert.equal(called, 2);
    });

    it("should register 'once' handler and emit just once", function() {
        const ee = new EventEmitter();

        let called = 0;

        const handler = function(data) {
            ++called;
        };

        ee.once("event", handler);

        ee.emit("event");
        ee.emit("event");

        assert.equal(called, 1);
    });

    it("should unregister handler", function() {
        const ee = new EventEmitter();

        let called = 0;

        const handler = function(data) {
            ++called;
        };

        ee.on("event", handler);
        ee.emit("event");
        ee.off("event", handler);
        ee.emit("event");

        assert.equal(called, 1);
    });

    it("should accept only function handlers", function() {
        const ee = new EventEmitter();

        assert.throws(function() {
            ee.on("event", 1);
        });
    });

    it("should not register same hadler twice for same event", function() {
        const ee = new EventEmitter();

        let called = 0;

        const handler = function(data) {
            ++called;
        };

        ee.on("event", handler);
        ee.on("event", handler);
        ee.emit("event");

        assert.equal(called, 1);
    });
});
