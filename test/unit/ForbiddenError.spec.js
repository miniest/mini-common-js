/* eslint-env mocha */

import { ForbiddenError } from "../../ForbiddenError.js";
import { MiniError } from "../../MiniError.js";
import chai from "chai";

describe("ForbiddenError", () => {
    it("Instantiate", async () => {
        const e = new ForbiddenError();

        chai.assert.instanceOf(e, ForbiddenError);
        chai.assert.instanceOf(e, MiniError); // extended
        chai.assert.equal(e.httpStatusCode, 403);
    });
});
