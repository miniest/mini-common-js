/* eslint-env mocha */

import { Server } from "../../Server.js";
import chai from "chai";

const assert = chai.assert;

describe("Server", function() {
    it("instantiate", async function() {
        const s = new Server();

        assert.isObject(s);
    });
});
