/* eslint-env mocha */

import { AuthorizationError } from "../../AuthorizationError.js";
import { MiniError } from "../../MiniError.js";
import chai from "chai";

describe("AuthorizationError", () => {
    it("Instantiate", async () => {
        const e = new AuthorizationError();

        chai.assert.instanceOf(e, AuthorizationError);
        chai.assert.instanceOf(e, MiniError); // extended

        chai.assert.equal(e.httpStatusCode, 401);
    });
});
