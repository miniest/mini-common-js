/* eslint-env mocha */

import { MiniError } from "../../MiniError.js";
import chai from "chai";

const assert = chai.assert;

describe("MiniError", () => {
    it("Instantiate", async () => {
        const e = new MiniError();

        assert.isDefined(e);

        chai.assert.equal(e.httpStatusCode, 500);
    });

    it("should log to logger", async function() {
        const e = new MiniError("TEST");

        let result = null;
        const logFn = function(data) {
            result = data;
        };

        e.logTo(logFn);

        assert.isNotNull(result);
    });

    it("should throw on throw", async function() {
        const e = new MiniError("TEST");

        assert.throws(function() {
            e.throw();
        });
    });

    it("should log error and cause", function() {
        const e1 = new MiniError("Test", new Error("cause"));

        e1.logTo(function(...data) {
            assert.lengthOf(data, 3);
        });

        const e2 = new MiniError("Test");

        e2.logTo(function(...data) {
            assert.lengthOf(data, 2);
        });
    });
});
