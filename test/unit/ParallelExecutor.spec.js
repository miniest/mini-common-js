/* eslint-env mocha */

import ParallelExecutor from "../../ParallelExecutor.js";

describe("test", async () => {
    it("test", async () => {
        const pe = new ParallelExecutor(5);

        for (let i = 0; i < 100; ++i) {
            pe.add((id) => {
                return new Promise((ok) => {
                    // console.log("starting", id);
                    setTimeout(() => {
                        // console.log("done", id);
                        ok();
                    }, 1);
                });
            }, i);

            // console.log("scheduled", i);
        }

        await Promise.all(pe.promises);
        // console.log("done");
    });
});
