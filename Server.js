import http from "http";
import express from "express";

function noop() {}

/**
 * Api server. Handles connections and modules.
 * Allows to register and use api server modules/routers.
 */
export class Server {
    static STARTING = "Starting";
    static RUNNING = "Running";
    static STOPPED = "Stopped";
    static STOPPING = "Stopping";

    /**
     * Creates instance of the Server.
     *
     * @param {Object} opts options
     * @param opts.host ip to bind to
     * @param opts.port port to listen on
     * @param opts.logger logger to user for logging. If none is specified server does not log at all.
     * @param opts.accessLogger logger used for logging individual requests (access log)
     */
    constructor(opts = {}) {
        this.accessLog = null;
        this.app = express();
        this.connections = [];
        this.log = null;
        this.server = http.createServer(this.app);
        this.status = Server.STOPPED;
        this.host = opts.host || "127.0.0.1";
        this.port = opts.port || "8080";
        this.log = opts.logger || {
            trace: noop,
            debug: noop,
            info: noop,
            warn: noop,
            error: noop,
        };

        this.accessLog = opts.accessLogger || {
            trace: noop,
            debug: noop,
            info: noop,
            warn: noop,
            error: noop,
        };

        this.app.use((req, res, next) => {
            res.start_time = Date.now();

            res.on("finish", () => {
                this.accessLog.info(
                    "method=%s url=%s dur_ms=%d status=%s",
                    req.method || "UNKN",
                    req.originalUrl || "UNKN",
                    Date.now() - res.start_time,
                    res.statusCode,
                );
            });

            next();
        });

        this.server.on("connection", (connection) => {
            this.connections.push(connection);

            connection.on("close", () => {
                this.connections = this.connections.filter(
                    (c) => c !== connection,
                );
            });
        });
    }

    /**
     * Starts the server operation.
     *
     * @async
     */
    async start() {
        this.log.trace("Starting service: %s:%s", this.host, this.port);
        if (this.status !== Server.STOPPED && this.status !== Server.STARTING) {
            this.log.trace("Server is not stopped, cannot start...");
            return;
        }

        this.status = Server.STARTING;
        return new Promise((ok, fail) => {
            const listenErrorHandler = (e) => {
                if (e.code === "EADDRINUSE") {
                    this.log.trace("Port occupied, trying again...");
                    setTimeout(() => this.start(), 1000);
                } else {
                    this.log.error(e);
                    fail(e);
                }
            };

            this.server.once("error", listenErrorHandler);
            this.server.listen(this.port, this.host, () => {
                this.log.trace("HttpServer started...");
                this.status = Server.RUNNING;
                this.server.off("error", listenErrorHandler);
                ok();
            });
        });
    }

    /**
     * Stops the server operation.
     *
     * @async
     */
    async stop() {
        this.log.trace("Stoping service: %s:%s", this.host, this.port);
        if (this.status !== Server.RUNNING) {
            this.log.trace("Server is not running, cannot stop...");
        }

        this.status = Server.STOPPING;
        this.connections.forEach((c) => c.destroy());

        await new Promise((ok, fail) => {
            this.server.on("close", () => {
                this.log.trace("Server close event");
                ok();
            });

            this.server.close(() => {
                this.log.trace("HttpServer stopped...", this.server.listening);
                this.status = Server.STOPPED;
            });
        });
    }

    /**
     * Adds a module router.
     *
     * @async
     *
     * @param {String} path module path
     * @param {routers.Router} router instance of the Router or its children
     */
    async addRouter(path, router) {
        this.app.use(path, router);

        return this;
    }

    /**
     * Adds a midleware to the server. Middleware follows sematics of common
     * expressjs midleware.
     *
     * @async
     *
     * @param {function(res, req, next)}  middleware middleware to use
     */
    async addMiddleware(middleware) {
        this.app.use(middleware);
    }
}
