export default class ParallelExecutor {
    #maxParallel;

    #currentRunning;

    #promises;

    #wakers;

    constructor(maxParallel = 3) {
        this.#maxParallel = maxParallel;
        this.#currentRunning = 0;
        this.#promises = [];
        this.#wakers = [];
    }

    get promises() {
        return this.#promises;
    }

    add(fn, ...params) {
        if (this.#currentRunning < this.#maxParallel) {
            ++this.#currentRunning;

            return this.#promises.push(
                fn(...params).finally(() => {
                    --this.#currentRunning;
                    return this.#wakers.shift()?.();
                }),
            );
        } else {
            const p = new Promise((ok) => {
                this.#wakers.push(ok);
            }).then(() => {
                return fn(...params).finally(() => {
                    --this.#currentRunning;
                    return this.#wakers.shift()?.();
                });
            });

            this.#promises.push(p);
            return p;
        }
    }
}
