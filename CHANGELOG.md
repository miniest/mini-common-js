# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.15](https://gitlab.com/miniest/mini-common-js/compare/v0.0.14...v0.0.15) (2021-01-26)


### Features

* add ParallelExecutor ([2e06009](https://gitlab.com/miniest/mini-common-js/commit/2e060094c8281b679f880eae9b7188dcc06874d3))

## [0.0.14](https://gitlab.com/miniest/mini-common-js/compare/v0.0.13...v0.0.14) (2020-01-03)


### Features

* implement EventEmitter ([50cac9c](https://gitlab.com/miniest/mini-common-js/commit/50cac9cf94edf52639ab8f960e0259000db521c7))



## [0.0.13](https://gitlab.com/miniest/mini-common-js/compare/v0.0.12...v0.0.13) (2019-12-09)


### Features

* change format of error logging ([6b10b90](https://gitlab.com/miniest/mini-common-js/commit/6b10b90aa798cfc814518f1dbf3a2ec76c2e2049))



## [0.0.12](https://gitlab.com/miniest/mini-common-js/compare/v0.0.11...v0.0.12) (2019-12-03)


### Bug Fixes

* intialization of class properties in Server constructor ([0c16b27](https://gitlab.com/miniest/mini-common-js/commit/0c16b276523c2b5fc65433ad516025068f6a970f))



## [0.0.11](https://gitlab.com/miniest/mini-common-js/compare/v0.0.10...v0.0.11) (2019-12-02)



## [0.0.10](https://gitlab.com/miniest/mini-common-js/compare/v0.0.9...v0.0.10) (2019-12-02)


### Features

* updated MiniError to be comaptible with mini-logging 1.0 ([76256fe](https://gitlab.com/miniest/mini-common-js/commit/76256fe281dff0401f9f149150ac16ea4076fba1))


### BREAKING CHANGES

* MiniError.logTo changed in incompatible way.



## [0.0.9](https://gitlab.com/miniest/mini-common-js/compare/v0.0.8...v0.0.9) (2019-10-24)



## [0.0.8](https://gitlab.com/miniest/mini-common-js/compare/v0.0.7...v0.0.8) (2019-10-24)


### Bug Fixes

* Fixed wrong stage name in CI pipeline ([a6e1fbe](https://gitlab.com/miniest/mini-common-js/commit/a6e1fbecaddfc1840974c299171b32de03fa427a))



## [0.0.7](https://gitlab.com/miniest/mini-common-js/compare/v0.0.7-0...v0.0.7) (2019-10-14)



## [0.0.7-0](https://gitlab.com/miniest/mini-common-js/compare/v0.0.4...v0.0.7-0) (2019-10-14)


### Bug Fixes

* removed unneeded httpStatusCode assignment ([85a2015](https://gitlab.com/miniest/mini-common-js/commit/85a2015e5633f54dbe517a61c2b379ca25c27132))


### Features

* Request access log ([72193f6](https://gitlab.com/miniest/mini-common-js/commit/72193f65b9c29071c9f2eff35dc50eeb1f989139))



## [0.0.4](https://gitlab.com/miniest/mini-common-js/compare/v0.0.3...v0.0.4) (2019-10-10)


### Features

* Http Server implementation added ([89b370d](https://gitlab.com/miniest/mini-common-js/commit/89b370dbe5f62e9552ed8c5ef75ff0c383334305))



## [0.0.3](https://gitlab.com/miniest/mini-common-js/compare/v0.0.2...v0.0.3) (2019-09-30)


### Bug Fixes

* Added required stage test ([5d583e8](https://gitlab.com/miniest/mini-common-js/commit/5d583e886778240dbe673ed4e798aa963fb16d5c))



## [0.0.2](https://gitlab.com/miniest/mini-common-js/compare/v0.0.1...v0.0.2) (2019-09-30)


### Features

* MiniError added ([3b4d2a9](https://gitlab.com/miniest/mini-common-js/commit/3b4d2a9808963207580d4990e17d6a6e5375aa0f))



## [0.0.1](https://gitlab.com/miniest/mini-common-js/compare/f9ff03b6fb09e358ea8395ca69bc8b57c7a2b7fa...v0.0.1) (2019-09-30)


### Features

* MongoDb tools added ([f9ff03b](https://gitlab.com/miniest/mini-common-js/commit/f9ff03b6fb09e358ea8395ca69bc8b57c7a2b7fa))
