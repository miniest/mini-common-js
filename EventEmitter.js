/**
 * Weak map holdin listeners
 */
const listeners = new WeakMap();

/**
 * Event Emitter.
 *
 * Allows to listen and trigger events.
 *
 * It uses weak map for registered handlers, so it should prevent
 * memory leaks.
 */
export class EventEmitter {
    constructor() {
        listeners.set(this, new Map());
    }

    /**
     * Shortcut to get own listeners
     *
     * @private
     */
    get _listeners() {
        // entry in the  weak map should live as long as this object lives
        return listeners.get(this);
    }

    /**
     * Register handler for event
     *
     * @param {string} eventName - name of the event
     * @param {function} handler - function that is called when event triggers
     * @param {boolean} once - indicates if handler should be triggered just once
     */
    on(eventName, handler, once = false) {
        if (typeof handler !== "function") {
            throw new Error("handler is not a function");
        }

        if (!this._listeners.has(eventName)) {
            this._listeners.set(eventName, new Map());
        }
        this._listeners.get(eventName).set(handler, once);
    }

    /**
     * Regiter handlre for event only once.
     * Shortcut for "on" with once = true
     *
     * @param {string} eventName - name of the event
     * @param {function} handler - function that is called when event triggers
     */
    once(eventName, handler) {
        this.on(eventName, handler, true);
    }

    /**
     * Unregister handler from listenning on event
     *
     * @param {string} eventName - name of the event
     * @param {function} handler - function that is called when event triggers
     */
    off(eventName, handler) {
        this._listeners.has(eventName) &&
            this._listeners.get(eventName).delete(handler);
    }

    /**
     * Emits event to all listeners. Order of invocation is not guaranteed.
     * Handlers marked as "once" are removed from listenners after invocation.
     *
     * @param {string} eventName - name of event to invoke handlers for
     * @param {object} [data=undefined] - data passed to the handler
     */
    emit(eventName, data) {
        this._listeners.has(eventName) &&
            this._listeners.get(eventName).forEach((v, k, map) => {
                k(data);

                v && map.delete(k);
            });
    }
}
