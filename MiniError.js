/**
 * Mase error class for miniest people.
 */
export class MiniError extends Error {
    /**
     * Base constructor
     *
     * @param {string} msg - message associated with the error
     * @param {Error} cause - nested cause of the error
     */
    constructor(msg, cause) {
        super(msg);

        this.name = this.constructor.name;
        this.cause = cause;

        // TODO dont know if this is needed
        // Error.captureStackTrace(this, this.constructor);
    }

    /**
     * Http status code associated with this kind of error
     *
     * @return {number} errorCode
     */
    get httpStatusCode() {
        return 500;
    }

    /**
     * Enables MiniError to be thrown in composed functions.
     *
     * @return {MiniError} this
     */
    throw() {
        throw this; // eslint-disable-line no-throw-literal
    }

    /**
     * Logs the error to the logger.
     *
     * @param {function} loggerFn - function to log by. Use logger instance 'infoFn' or similar
     *
     * @return {MiniError} this
     */
    logTo(logFn) {
        // TODO assert to be function and defined
        if (this.cause) {
            const c = this.cause;
            logFn("Error: %s:\ncaused by: %s", this.stack, c.stack);
        } else {
            logFn("Error: %s", this.stack);
        }

        return this;
    }

    toJSON() {
        const r = {
            name: this.name,
            message: this.message,
        };

        if (this.cause) {
            if (this.cause.toJSON) {
                r.cause = this.cause.toJSON();
            } else if (this.cause instanceof Error) {
                r.cause = {
                    name: this.cause.constructor.name,
                    message: this.cause.message,
                };
            } else {
                r.cause = this.cause;
            }
        }
        return r;
    }
}
